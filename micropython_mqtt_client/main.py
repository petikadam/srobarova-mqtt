#Network section
import network
sta_if = network.WLAN(network.STA_IF);
sta_if.active(True)
sta_if.scan()# Scan for available access points
sta_if.connect("name", "password") # Connect to an AP
sta_if.isconnected()


from machine import Pin
ledka1 = Pin(18, Pin.OUT)
ledka2 = Pin(21, Pin.OUT)


#MQTT section
import time
import json
from mqttsimple import MQTTClient


def sub_cb(topic, msg):
  #print((topic, msg))
  my_json = msg.decode('utf8').replace("'", '"')
  print(my_json)
  my_json = json.loads(my_json)
  if my_json["type"] == 0:
      ledka1.off()
      ledka2.off()
  elif  my_json["type"] == 1:
      ledka1.on()
      ledka2.off()
  elif  my_json["type"] == 2:
      ledka1.off()
      ledka2.on()
  elif  my_json["type"] == 3: 
      ledka1.on()
      ledka2.on()
  
  
  
mqtt_server = 'broker.mqttdashboard.com'
client_id = b'subscriber'
topic_sub = b'srobarova/prezentacia'

client = MQTTClient(client_id, mqtt_server)
client.set_callback(sub_cb)
client.connect()
client.subscribe(topic_sub)


#logic    
while True:
    message = client.check_msg()
    




