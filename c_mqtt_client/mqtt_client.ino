#include <EspMQTTClient.h>

const char* WIFI_SSID = "";
const char* WIFI_PASS = "";

const char* MQTT_BROKER = "broker.hivemq.com";
const char* MQTT_CLIENT_NAME = "client-name";
const short MQTT_PORT = 1883;

const char* TOPIC = "srobarova/prezentacia";

const int button1Pin = 5;
const int button2Pin = 18;
const int button3Pin = 19;
const int button4Pin = 21;

// create MQTT client
EspMQTTClient client(
  WIFI_SSID,
  WIFI_PASS,
  MQTT_BROKER,
  MQTT_CLIENT_NAME,
  MQTT_PORT
);

void setup() {
  Serial.begin(9600);

  pinMode(button1Pin, INPUT);
  pinMode(button2Pin, INPUT);
  pinMode(button3Pin, INPUT);
  pinMode(button4Pin, INPUT);

  client.enableDebuggingMessages(); // to see what is going on
}

void onConnectionEstablished() {
  // great place to create subscriptions
}

void loop() {
  client.loop();
  
  int pushedButton = getPushedButton();
  
  if (pushedButton == 0) {
    return; // no button was pushed
  }
  
  Serial.print("Pushed button: ");
  Serial.println(pushedButton);

  // create payload message as JSON
  String message = createJSON(pushedButton);

  client.publish(TOPIC, message);

  Serial.print("Message '");
  Serial.print(message);
  Serial.println("' published");

}

String createJSON(int pushedButton) { 
  String json;
  json += "{\"type\":";
  json += pushedButton - 1;
  json +=  "}";
  return json;
}

int getPushedButton() {
  int pushedButton = 0;
  
  int button1State = digitalRead(button1Pin);
  int button2State = digitalRead(button2Pin);
  int button3State = digitalRead(button3Pin);
  int button4State = digitalRead(button4Pin);
  while(button1State == HIGH || button2State == HIGH ||
        button3State == HIGH || button4State == HIGH)
  {
    if (button1State == HIGH){
      pushedButton = 1;   
    } else if (button2State == HIGH) {
      pushedButton = 2;   
    } else if (button3State == HIGH) {
      pushedButton = 3;   
    } else if (button4State == HIGH) {
      pushedButton = 4;   
    }
    button1State = digitalRead(button1Pin);
    button2State = digitalRead(button2Pin);
    button3State = digitalRead(button3Pin);
    button4State = digitalRead(button4Pin);
  }

  return pushedButton;
}
